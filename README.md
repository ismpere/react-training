# TypeScript Next.js React training

This is a really simple project that shows the usage of Next.js with TypeScript.

## Usage

In order to run the application, is necessary to execute the following commands:

```bash
npm install
npm update
npm run dev
```
