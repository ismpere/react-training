import { createSelector } from "@reduxjs/toolkit";
import { RootState } from "../../app/store";

export const autocompleteBreweries = (state: RootState) =>
  state.autocompleteBreweries;

export const autocompleteBreweriesSelector = createSelector(
  autocompleteBreweries,
  (state) => state
);
