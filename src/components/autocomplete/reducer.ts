import { createReducer } from "@reduxjs/toolkit";
import { fetchAutocompleteBreweries, resetAutocomplete } from ".";

export type Brewery = {
  id: string;
  name: string;
  city: string;
  country: string;
  website_url: string;
};

export type BreweriesAutocompleteState = {
  options: { value: string; label: string }[];
  inputBrewery: string;
  selectedBrewery: { value: string; label: string };
  pending: boolean;
  error: boolean;
};

const initialState: BreweriesAutocompleteState = {
  options: [],
  inputBrewery: "",
  selectedBrewery: null,
  pending: false,
  error: false,
};

export const autocompleteBreweriesReducer = createReducer(
  initialState,
  (builder) => {
    builder
      .addCase(fetchAutocompleteBreweries.pending, (state) => {
        state.pending = true;
      })
      .addCase(fetchAutocompleteBreweries.fulfilled, (state, action) => {
        state.pending = false;
        state.options = action.payload.breweries;
        state.inputBrewery = action.payload.input;
      })
      .addCase(fetchAutocompleteBreweries.rejected, (state) => {
        state.pending = false;
        state.error = true;
      })
      .addCase(resetAutocomplete, (state) => {
        state.options = [];
        state.inputBrewery = "";
      });
  }
);

export default autocompleteBreweriesReducer;
