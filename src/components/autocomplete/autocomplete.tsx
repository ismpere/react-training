import TextLink from "@kiwicom/orbit-components/lib/TextLink";
import Link from "next/link";
import React from "react";
import Select from "react-select";
import { useAppDispatch, useAppSelector } from "../../app/hooks";
import { fetchAutocompleteBreweries, resetAutocomplete } from "./actions";
import { autocompleteBreweriesSelector } from "./selectors";
import classes from "./styles.module.scss";

const autocomplete: React.FC = () => {
  const dispatch = useAppDispatch();
  const { options, inputBrewery, selectedBrewery } = useAppSelector(
    autocompleteBreweriesSelector
  );

  const formatOptionLabel = ({ value, label }) => (
    <div>
      <Link href={`/brewery/${value}`}>
        <TextLink>{label}</TextLink>
      </Link>
    </div>
  );

  const filterOption = () => {
    return true;
  };

  function handleInputChange(input: string) {
    dispatch(fetchAutocompleteBreweries(input));
  }

  function handleSelectChange() {
    dispatch(resetAutocomplete());
  }

  return (
    <div className={classes.autocomplete_select}>
      <Select
        placeholder="Look for breweries..."
        options={options}
        onInputChange={handleInputChange}
        formatOptionLabel={formatOptionLabel}
        onChange={handleSelectChange}
        inputValue={inputBrewery}
        value={selectedBrewery}
        filterOption={filterOption}
      />
    </div>
  );
};

export default autocomplete;
