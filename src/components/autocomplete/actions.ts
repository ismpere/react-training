import { createAction, createAsyncThunk } from "@reduxjs/toolkit";
import { Brewery } from ".";
import http from "../../api/http-common";
import { store } from "../../app/store";
import { fetchBreweries } from "../../features/breweries";

const AUTOCOMPLETE_MAX_RESULTS = 5;

function checkEmptyBreweries() {
  return store.getState().breweries.data.length == 0;
}

async function filterBreweriesByName(name: string | string[], { dispatch }) {
  if (name == "") {
    return [];
  }
  if (checkEmptyBreweries()) {
    await dispatch(fetchBreweries());
  }

  let nameRegex = new RegExp(name.toString(), "i");
  let breweries: Brewery[] = store.getState().breweries.data;
  let breweriesFiltered: Brewery[] = breweries.filter((brewery) =>
    nameRegex.test(brewery.name)
  );

  let breweriesMap = breweriesFiltered
    .slice(0, AUTOCOMPLETE_MAX_RESULTS)
    .map(function (brewery: Brewery) {
      return { label: brewery.name, value: brewery.id };
    });
  return { breweries: breweriesMap, input: name };
}

async function fetchBreweriesByQuery(name: string | string[], { dispatch }) {
  if (name == "") {
    return [];
  }
  const response = await http.get("/breweries/autocomplete", {
    params: {
      query: name,
    },
  });

  let breweries: Brewery[] = response.data;
  let breweriesMap = breweries
    .slice(0, AUTOCOMPLETE_MAX_RESULTS)
    .map(function (brewery: Brewery) {
      return { label: brewery.name, value: brewery.id };
    });
  return { breweries: breweriesMap, input: name };
}

export const fetchAutocompleteBreweries = createAsyncThunk(
  "autocomplete/fetchAutocompleteBreweries",
  fetchBreweriesByQuery
);

export const resetAutocomplete = createAction("autocomplete/reset");
