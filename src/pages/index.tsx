import Heading from "@kiwicom/orbit-components/lib/Heading";
import TextLink from "@kiwicom/orbit-components/lib/TextLink";
import MUIDataTable, { MUIDataTableColumn } from "mui-datatables";
import Link from "next/link";
import React, { useEffect } from "react";
import { useAppDispatch, useAppSelector } from "../app/hooks";
import { breweriesSelector, fetchBreweries } from "../features/breweries";
import classes from "../styles/global.module.scss";

const breweries: React.FC = () => {
  const dispatch = useAppDispatch();
  const { data, pending, error } = useAppSelector(breweriesSelector);

  const columns: MUIDataTableColumn[] = [
    {
      name: "id",
      label: "Id",
      options: {
        display: false,
      },
    },
    {
      name: "name",
      label: "Name",
      options: {
        customBodyRenderLite: (dataIndex) => {
          return (
            <Link href={`/brewery/${data[dataIndex].id}`}>
              <TextLink>{data[dataIndex].name}</TextLink>
            </Link>
          );
        },
      },
    },
    {
      name: "brewery_type",
      label: "Type",
    },
    {
      name: "country",
      label: "Country",
    },
    {
      name: "city",
      label: "City",
    },
    {
      name: "phone",
      label: "Phone number",
    },
    {
      name: "website_url",
      label: "Website",
      options: {
        filter: false,
        customBodyRender: (value: string) => (
          <TextLink href={value}>{value}</TextLink>
        ),
      },
    },
  ];

  useEffect(() => {
    dispatch(fetchBreweries());
  }, [fetchBreweries]);

  return (
    <div>
      {data.length == 0 && (
        <div className={classes.index_header}>
          <Heading>{pending && "Loading..."}</Heading>
          <Heading>{error && "Oops, something went wrong"}</Heading>
        </div>
      )}
      {data.length > 0 && (
        <div>
          <div className={classes.index_header}>
            <Heading>Breweries</Heading>
          </div>
          <div>
            <MUIDataTable
              title={"Breweries datatable"}
              data={data}
              columns={columns}
            />
          </div>
        </div>
      )}
    </div>
  );
};

export default breweries;
