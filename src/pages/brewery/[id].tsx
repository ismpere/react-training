import ButtonLink from "@kiwicom/orbit-components/lib/ButtonLink";
import Heading from "@kiwicom/orbit-components/lib/Heading";
import Layout, { LayoutColumn } from "@kiwicom/orbit-components/lib/Layout";
import Text from "@kiwicom/orbit-components/lib/Text";
import TextLink from "@kiwicom/orbit-components/lib/TextLink";
import { useRouter } from "next/dist/client/router";
import Head from "next/head";
import Link from "next/link";
import { useEffect } from "react";
import { useAppDispatch, useAppSelector } from "../../app/hooks";
import { brewerySelector, fetchBrewery } from "../../features/brewery";
import classes from "../../features/brewery/styles.module.scss";

export async function getServerSideProps(context) {
  return {
    props: {},
  };
}

const brewery: React.FC = () => {
  const dispatch = useAppDispatch();
  const { data, pending, error } = useAppSelector(brewerySelector);

  const router = useRouter();
  const { id } = router.query;

  useEffect(() => {
    dispatch(fetchBrewery(id));
  }, [fetchBrewery, id]);

  return (
    <div>
      <Head>
        <title>{id}</title>
      </Head>
      {!data && (
        <div className={classes.brewery_header}>
          <Heading>{pending && "Loading..."}</Heading>
          <Heading>{error && "Oops, something went wrong"}</Heading>
        </div>
      )}
      <div className={classes.brewery_header}>
        <Heading>{data && data.name}</Heading>
      </div>
      {data && !pending && !error && (
        <div className={classes.brewery_layout}>
          <Layout type="Booking">
            <LayoutColumn>
              <div>
                <Text weight="bold">Name: </Text>
                <Text>{data.name}</Text>
              </div>
            </LayoutColumn>
            <LayoutColumn>
              <div>
                <Text weight="bold">Country: </Text>
                <Text>{data.country}</Text>
              </div>
            </LayoutColumn>
            <LayoutColumn>
              <div>
                <Text weight="bold">City: </Text>
                <Text>{data.city}</Text>
              </div>
            </LayoutColumn>
            <LayoutColumn>
              <div>
                <Text weight="bold">{data.website_url && "Website:"}</Text>
                <TextLink href={data && data.website_url}>
                  {data && data.website_url}
                </TextLink>
              </div>
            </LayoutColumn>
          </Layout>
        </div>
      )}
      <div className={classes.breweries_button}>
        <Link href={"/"}>
          <ButtonLink>{"< Breweries list"}</ButtonLink>
        </Link>
      </div>
    </div>
  );
};

export default brewery;
