import type { AppProps } from "next/app";
import { Provider } from "react-redux";
import { store } from "../app/store";
import Autocomplete from "../components/autocomplete/autocomplete";

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <Provider store={store}>
      <Autocomplete></Autocomplete>
      <Component {...pageProps} />
    </Provider>
  );
}

export default MyApp;
