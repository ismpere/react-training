import { createReducer } from "@reduxjs/toolkit";
import { fetchBreweries } from "./actions";

export type BreweriesState = {
  data: [];
  pending: boolean;
  error: boolean;
};

const initialState: BreweriesState = {
  data: [],
  pending: false,
  error: false,
};

export const breweriesReducer = createReducer(initialState, (builder) => {
  builder
    .addCase(fetchBreweries.pending, (state) => {
      state.pending = true;
    })
    .addCase(fetchBreweries.fulfilled, (state, { payload }) => {
      state.pending = false;
      state.data = payload;
    })
    .addCase(fetchBreweries.rejected, (state) => {
      state.pending = false;
      state.error = true;
    });
});

export default breweriesReducer;
