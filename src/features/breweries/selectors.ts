import { createSelector } from "@reduxjs/toolkit";
import { RootState } from "../../app/store";

export const selectBreweries = (state: RootState) => state.breweries;

export const breweriesSelector = createSelector(
  selectBreweries,
  (state) => state
);
