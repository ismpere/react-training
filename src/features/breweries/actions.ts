import { createAsyncThunk } from "@reduxjs/toolkit";
import http from "../../api/http-common";

export const fetchBreweries = createAsyncThunk(
  "breweries/fetchBreweries",
  async () => {
    const response = await http.get("/breweries");

    return response.data;
  }
);
