import { createSelector } from "@reduxjs/toolkit";
import { RootState } from "../../app/store";

export const selectBrewery = (state: RootState) => state.brewery;

export const brewerySelector = createSelector(selectBrewery, (state) => state);
