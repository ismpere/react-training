import { createReducer } from "@reduxjs/toolkit";
import { fetchBrewery } from "./actions";

export type Brewery = {
  id: string;
  name: string;
  brewery_type: string;
  city: string;
  country: string;
  website_url: string;
  phone: string;
};

export type BreweryState = {
  data: Brewery;
  pending: boolean;
  error: boolean;
};

const initialState: BreweryState = {
  data: null,
  pending: false,
  error: false,
};

export const breweryReducer = createReducer(initialState, (builder) => {
  builder
    .addCase(fetchBrewery.pending, (state) => {
      state.pending = true;
    })
    .addCase(fetchBrewery.fulfilled, (state, action) => {
      state.pending = false;
      state.data = action.payload;
    })
    .addCase(fetchBrewery.rejected, (state) => {
      state.pending = false;
      state.error = true;
    });
});

export default breweryReducer;
