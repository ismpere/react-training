import { createAsyncThunk } from "@reduxjs/toolkit";
import { Brewery } from ".";
import http from "../../api/http-common";
import { store } from "../../app/store";

function obtainBrewery(id: string | string[]) {
  let breweries = store.getState().breweries.data;
  let brewery: Brewery = breweries.find(
    (brewery: Brewery) => brewery.id.toString() === id
  );
  return brewery;
}

export const fetchBrewery = createAsyncThunk(
  "breweries/fetchBrewery",
  async (id: string | string[]) => {
    let brewery: Brewery = obtainBrewery(id);
    if (brewery) {
      return brewery;
    }

    const response = await http.get("/breweries/" + id);
    return response.data;
  }
);
