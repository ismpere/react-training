import { Action, configureStore, ThunkAction } from "@reduxjs/toolkit";
import { autocompleteBreweriesReducer } from "../components/autocomplete";
import { breweriesReducer } from "../features/breweries";
import { breweryReducer } from "../features/brewery";

export const store = configureStore({
  reducer: {
    breweries: breweriesReducer,
    brewery: breweryReducer,
    autocompleteBreweries: autocompleteBreweriesReducer,
  },
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;
